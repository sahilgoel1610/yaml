require "google/api_client"
require "google_drive"
require "csv"

namespace :import_transalation do
  desc "Download conversion file from google drive and udpate the locales files"
  task :update_locales => :environment do

    # Read file from cloud
    session = GoogleDrive.saved_session("config.json")
    spread_sheet = session.spreadsheet_by_title("Translations")

    spread_sheet.worksheets.each do |worksheet|
      worksheet_list = worksheet.list
      worksheet_list.keys[2..worksheet_list.keys.length].each do |language|  
      
        # make file and directory if it doesnt exist
        path = "config/locales/#{worksheet.title}/#{language}.yml"
        dir = File.dirname(path)

        unless File.directory?(dir)
           FileUtils.mkdir_p(dir)
        end


        # open a file by  overwriting the file @ specified path
        file = File.open(path,'w+')
        current_classes = []
        tabs = ""

        # loop over for each langugae
        worksheet_list.each do |row|

              classes = row["keys"].split(".")
              
              # form the class tree with tabs depending upon last class tree printed
              classes.each_with_index do |class_name,i|
                  tabs += "\t" if i > 0
                  if class_name != current_classes[i]
                      file.puts(tabs + class_name + ": ") if i < classes.length - 1 
                      current_classes[i] = class_name
                  end    
              end

            file.puts(tabs + current_classes.last + ": " + row[language])
            tabs = ""
        end
        
        # save the file
        file.close
      end
    end
  end

  desc "create CSV files from locales folder"
  task :create_csv_from_locales => :environment do
    folders = ['Homepage','Marketing']
    folders.each do |folder_name|
    data = {"keys" => []}
    

    # Extract Key from first file
    first_file = Dir.glob("config/locales/#{folder_name}/*.yml").first
    classes = []
    File.open(first_file).each do |row|
      splitted_row = row.gsub(" ","").gsub("\n","").split(":")
      classes[splitted_row.first.index(/[^\t ]/)] = splitted_row.first.gsub(/\t/,'')
      data["keys"] << classes.join(".") if splitted_row.count > 1
    end
    
    # Extract data from Yaml files
    Dir.glob("config/locales/#{folder_name}/*.yml") do |item|
      next if item == '.' or item == '..'
      f_name = item.split("/").last.split(".").first
      data[f_name] = []
      File.open(item).each do |row|
        data[f_name] << row.split(':').last.strip
      end
      data[f_name] = data[f_name].reject(&:empty?)
    end


    # Write the data into the CSV file
    i = 0
    CSV.open("config/locales/#{folder_name}/#{folder_name}.csv", "wb") do |csv|
      push = []
      (0..(data.values.first.length-1)).each do |i|
        data.values.each {|v| push << v[i]}
        csv << push
        push = []
      end
    end

  end

end